package main

import "log"

// Calculator exported function
func Calculator(action string, number1, number2 int) {

	switch action {

	case "sum", "SUM":
		log.Println(number1 + number2)
		break

	case "div":
		log.Println(number1 / number2)
		break
	}
}
