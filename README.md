# GOlang

# Installation on Ubuntu:

Un-tar gz file

```
tar -xzf go$VERSION.$OS-$ARCH.tar.gz
mv go ~/
```

Set environment variables like:

```
export GOPATH=$HOME/golang
export GOROOT=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
```

```
source ~/.bashrc
```

Check for go version:

```
go version
```