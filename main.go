package main

import (
	"errors"
	"log"
	// import statements are sorted by the 'imports' plugin
)

func main() {
	// var isActive bool
	// var str string

	enabled := false
	hello := "Hello"

	log.Println(hello, enabled)

	calc("", 1, 2)

	result := sum(1, 2)

	log.Println("Result :", result)

	product, err := div(1, 3)

	if err != nil {
		log.Println("error", err)
		return
	}

	if enabled {
		log.Println("enabled")
	}

	// Falsy values
	// string ""
	// bool false
	// int 0
	// ...

	log.Println(product)
}

func sum(n, n2 int) int {
	return n + n2
}

func div(n, n2 int) (int, error) {
	// return n / 2, nil
	return 0, errors.New("My error here")
}
